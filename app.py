import json
import os
import re
from collections import Counter

import requests
from slacker import Slacker
from flask import Flask, request

app = Flask(__name__)

ACCESS_TOKEN = os.environ.get('ACCESS_TOKEN', '')
HEROKU_BEARER = os.environ.get('HEROKU_BEARER', '')
SLACK_BOT_TOKEN = os.environ.get('SLACK_BOT_TOKEN', '')


class HerokuAPI:
    def __init__(self, heroku_bearer):
        self._headers = {
            'Content-Type': "application/json",
            'Accept': "application/vnd.heroku+json; version=3",
            'Authorization': "Bearer {}".format(heroku_bearer)
        }

    def restart_dyno(self, target_app, dyno_id):
        url = "https://api.heroku.com/apps/{target_app}/dynos/{dyno_id}".format(target_app=target_app,
                                                                                dyno_id=dyno_id)
        response = requests.delete(url, headers=self._headers)

        return response


def get_affected_dyno_id(events):
    error_counter = Counter()
    for ev in events:
        res = re.search('dyno=([^\s]+)', ev.get('message')).group(1)
        error_counter[res] += 1

    return error_counter.most_common(1)[0][0]


def send_slack_message(message):
    slack = Slacker(SLACK_BOT_TOKEN)
    slack.chat.post_message('#techevents', message, as_user='21buttons-backend')


@app.route('/apps/<string:app_id>/dyno-errors', methods=['POST'])
def hello_world(app_id):
    token = request.args.get('token')
    if token == ACCESS_TOKEN:
        json_data = json.loads(request.form.get('payload'))
        events = json_data.get('events')

        dyno_id = get_affected_dyno_id(events)

        response = HerokuAPI(heroku_bearer=HEROKU_BEARER).restart_dyno(app_id, dyno_id)

        print(response.content)

        message = "El dyno {} está un poco trambólico... ".format(dyno_id)
        if response.status_code == 202:
            message += "Reiniciado con éxito!"
        else:
            message += "No se ha podido reiniciar :(\n {}".format(response.status_code)

        send_slack_message(message)

        return response.content, response.status_code

    return '', 401


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
