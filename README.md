# Heroku Watchdog #

### What is this repository for? ###

This small applications is triggered by Papertrail webhooks when something is wrong, then analyzes the data to find the
 "bad" dyno and restarts it.

### How do I get set up? ###

* python 3.6
* pip install -r requirements.txt
* python app.py

### Who do I talk to? ###

* Jairo Vadillo (@jairo)
