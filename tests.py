import json

from app import get_affected_dyno_id


def test_get_affected_dyno_id():
    f = open('data.json', 'r')
    data_json = json.load(f)
    events = data_json.get('events')

    affected_dyno = get_affected_dyno_id(events)

    assert affected_dyno == 'web.37'
